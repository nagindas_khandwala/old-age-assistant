﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
namespace MyBot.Chatbot_Page
{
    public class SpellChecker
    {
         SqlConnection con;
         SqlCommand cmd;
         SqlDataReader result;
        //number of words in my database
         int i = 80368;
        private Dictionary<String, int> dict;
        private  Regex wrdregex;
        public SpellChecker()
        {
            dict = new Dictionary<String, int>();
            wrdregex = new Regex("[a-z]+", RegexOptions.Compiled);
            String conStr = ConfigurationManager.ConnectionStrings["con"].ConnectionString;
            con = new SqlConnection(conStr);
            con.Open();
            String cmdStr = "Select * from [Dictionary]";
            cmd = new SqlCommand(cmdStr, con);
            result = cmd.ExecuteReader();
            if (result.HasRows)
            {
                while (result.Read())
                {
                    dict.Add(result.GetValue(0).ToString(), i--);
                }
            }
        }
        public  String Checker(String Txt)
        {
            String[] words = Txt.Split(' ');
            String res = "";
            foreach (String s in words)
            {
                res += Correct(s) + " ";
            }
            return res;
        }
        public string Correct(string word)
        {
            if (string.IsNullOrEmpty(word))
                return word;

            word = word.ToLower();

           
            if (dict.ContainsKey(word))
                return word;

            List<String> list = Edits(word);
            Dictionary<string, int> candidates = new Dictionary<string, int>();

            foreach (string wordVariation in list)
            {
                if (dict.ContainsKey(wordVariation) && !candidates.ContainsKey(wordVariation))
                    candidates.Add(wordVariation, dict[wordVariation]);
            }

            if (candidates.Count > 0)
                return candidates.OrderByDescending(x => x.Value).First().Key;

            foreach (string item in list)
            {
                foreach (string wordVariation in Edits(item))
                {
                    if (dict.ContainsKey(wordVariation) && !candidates.ContainsKey(wordVariation))
                        candidates.Add(wordVariation, dict[wordVariation]);
                }
            }

            return (candidates.Count > 0) ? candidates.OrderByDescending(x => x.Value).First().Key : word;
        }

        private List<string> Edits(string word)
        {
            var splits = new List<Tuple<string, string>>();
            var transposes = new List<string>();
            var deletes = new List<string>();
            var replaces = new List<string>();
            var inserts = new List<string>();

            // Splits
            for (int i = 0; i < word.Length; i++)
            {
                var tuple = new Tuple<string, string>(word.Substring(0, i), word.Substring(i));
                splits.Add(tuple);
            }

            // Deletes
            for (int i = 0; i < splits.Count; i++)
            {
                string a = splits[i].Item1;
                string b = splits[i].Item2;
                if (!string.IsNullOrEmpty(b))
                {
                    deletes.Add(a + b.Substring(1));
                }
            }

            // Transposes
            for (int i = 0; i < splits.Count; i++)
            {
                string a = splits[i].Item1;
                string b = splits[i].Item2;
                if (b.Length > 1)
                {
                    transposes.Add(a + b[1] + b[0] + b.Substring(2));
                }
            }

            // Replaces
            for (int i = 0; i < splits.Count; i++)
            {
                string a = splits[i].Item1;
                string b = splits[i].Item2;
                if (!string.IsNullOrEmpty(b))
                {
                    for (char c = 'a'; c <= 'z'; c++)
                    {
                        replaces.Add(a + c + b.Substring(1));
                    }
                }
            }

            // Inserts
            for (int i = 0; i < splits.Count; i++)
            {
                string a = splits[i].Item1;
                string b = splits[i].Item2;
                for (char c = 'a'; c <= 'z'; c++)
                {
                    inserts.Add(a + c + b);
                }
            }

            return deletes.Union(transposes).Union(replaces).Union(inserts).ToList();
        }

    }
}