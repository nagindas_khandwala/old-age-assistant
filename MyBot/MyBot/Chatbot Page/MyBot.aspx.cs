﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using MyBot.Chatbot_Page;
using System.Web.UI.HtmlControls;
namespace MyBot
{
    public partial class MyBot : System.Web.UI.Page
    {

        String resp,usertext;
        SqlConnection con;
        SqlCommand cmd;

        public List<Labelinfo> labelinfo
        {
            get
            {
                object listoflabelinfo1 = ViewState["Labels1"];
                return (listoflabelinfo1 == null) ? null : (List<Labelinfo>)listoflabelinfo1;
            }
            set
            {
                ViewState["Labels1"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                labelinfo = new List<Labelinfo>();
                if (Session["UserName"] != null)
                {
                    lblbotname.Text = Session["BotName"].ToString();
                    Label1.Text = "Hi " + Session["UserName"] + ", How are you.";
                }
                else
                {
                    Response.Redirect("~/Login Page/Login.aspx");
                }
            }
        }
        
        protected void txtuser_TextChanged(object sender, EventArgs e)
        {
            usertext = txtuser.Text;
            usertext = usertext.ToLower();
            IsBadword badword = new IsBadword();
            Normalizer normalizing = new Normalizer();
            Request req = new Request();
            string norm = normalizing.normalized(usertext);
            string cleanstring = normalizing.cleanString(norm);
            if (badword.BadWord(cleanstring) > 0)
            {
                resp = badword.find();
                req.save_prev_input(usertext);
                Labels1(usertext, resp);
            }
            else
            {
                String responses = req.reqst(cleanstring);
                resp = req.Replaces(responses);
                req.save_prev_input(usertext);
                req.save_prev_output(resp);
                Labels1(usertext, resp);
            }
            LogChats();
            txtuser.Text = "";
            txtuser.Focus();
        }

        private void Labels1(String text1, String text2)
        {
            labelinfo.Add(new Labelinfo { Text1 = text1, CssClass = "chat-message", Text2 = text2 });
            foreach (Labelinfo str in labelinfo)
            {
                HtmlGenericControl div3 = new HtmlGenericControl("DIV");
                div3.Attributes.Add("runat", "server");
                div3.Attributes.Add("class", "chat user");
                HtmlGenericControl div4 = new HtmlGenericControl("DIV");
                div4.Attributes.Add("runat", "server");
                div4.Attributes.Add("class", "user-photo");
                Image img2 = new Image();
                img2.ImageUrl = "~/Images/user_male2-512.png";
                img2.CssClass = "img";
                div4.Controls.Add(img2);
                div3.Controls.Add(div4);
                div3.Controls.Add(new Label { Text = str.Text1, CssClass = str.CssClass });
                MainChat.Controls.Add(div3);

                HtmlGenericControl div1 = new HtmlGenericControl("DIV");
                div1.Attributes.Add("runat", "server");
                div1.Attributes.Add("class", "chat bot");
                HtmlGenericControl div2 = new HtmlGenericControl("DIV");
                div2.Attributes.Add("runat", "server");
                div2.Attributes.Add("class", "user-photo");
                Image img1 = new Image();
                img1.ImageUrl = "~/Images/FileBot-icon.png";
                img1.CssClass = "img";
                div2.Controls.Add(img1);
                div1.Controls.Add(div2);
                div1.Controls.Add(new Label { Text = str.Text2, CssClass = str.CssClass });
                MainChat.Controls.Add(div1);
            }
        }

        [System.Web.Script.Services.ScriptMethod]
        [System.Web.Services.WebMethod]
        public static List<String> GetComplete(String prefixText)
        {
            List<String> Dictionary = new List<String>();
            SqlConnection con;
            SqlCommand cmd;
            SqlDataReader dr;
            try
            {
                con = new SqlConnection();
                con.ConnectionString = ConfigurationManager.ConnectionStrings["con"].ConnectionString;
                con.Open();
                cmd = new SqlCommand("Select TOP 5 [words] from [Dictionary] where [words] like '" + prefixText + "%'", con);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Dictionary.Add(dr["words"].ToString());
                }
                con.Close();
                dr.Close();
            }
            catch (Exception ex) { }
            return Dictionary;
        }

        private void LogChats()
        {
            connect();
            cmd = new SqlCommand("procLogChats", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter USER = new SqlParameter("@username", Session["UserName"]);
            SqlParameter REQUEST = new SqlParameter("@request",usertext );
            SqlParameter RESPONSE = new SqlParameter("@response", resp);
            SqlParameter DATE_TIME = new SqlParameter("@date_time", DateTime.Now.ToString());

            cmd.Parameters.Add(USER);
            cmd.Parameters.Add(REQUEST);
            cmd.Parameters.Add(RESPONSE);
            cmd.Parameters.Add(DATE_TIME);

            cmd.ExecuteScalar();
            con.Close();
        }

        public void connect()
        {
            String conStr = ConfigurationManager.ConnectionStrings["con"].ConnectionString;
            con = new SqlConnection(conStr);
            con.Open();
        }

    }
}

[Serializable]
public class Labelinfo
{
    public string Text1 = "";
    public string Text2 = "";
    public string CssClass = "";
}