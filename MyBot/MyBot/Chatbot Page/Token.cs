﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace MyBot.Chatbot_Page
{
    public class Token
    {

        private List<string> token;
        private Dictionary<string, int> keyword;
        SqlConnection con;
        SqlCommand cmd;
        SqlDataReader dr;
        private int key;
        
        public Token()
        {
            token = new List<string>();
            keyword = new Dictionary<string, int>();
        }

        public void connect()
        {
            String strcon = ConfigurationManager.ConnectionStrings["con"].ConnectionString;
            con = new SqlConnection(strcon);
            con.Open();
        }

        public void trikeyword()
        {
            keyword.Clear();
            int i = 1;
            connect();
            string strcmd = "select distinct(keywords) from AllTriKeywords";
            cmd = new SqlCommand(strcmd, con);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                keyword.Add(dr[0].ToString(), i++);
            }
            con.Close();
            dr.Close();
        }

        public void bikeyword()
        {
            keyword.Clear();
            int i = 1;
            connect();
            string strcmd = "select distinct(keyword) from AllBiKeywords";
            cmd = new SqlCommand(strcmd, con);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                keyword.Add(dr[0].ToString(), i++);
            }
            con.Close();
            dr.Close();
        }

        public void monokeyword()
        {
            keyword.Clear();
            connect();
            cmd = new SqlCommand("select distinct Keyword,Response_ID from Keywords",con);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                keyword.Add(dr.GetValue(0).ToString(),Convert.ToInt32(dr.GetValue(1)));
            }
            con.Close();
            dr.Close();
        }

        public List<string> Tokens(String Text)
        {
            token.Clear();
            string[] words = Text.Split(' ');
            foreach (string str in words)
            {
                if (keyword.ContainsKey(str))
                {
                    token.Add(str);
                }
            }
            return token;
        }

        public int Findkeys(string Text)
        {
            string[] words = Text.Split(' ');
            foreach (String s in words)
            {
                if (keyword.ContainsKey(s))
                {
                    key = keyword[s];
                }
            }
            return key;
        }

    }
}