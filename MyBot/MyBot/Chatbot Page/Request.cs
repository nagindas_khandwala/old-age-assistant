﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using MyBot.Chatbot_Page;
namespace MyBot.Chatbot_Page
{
    public class Request
    {
        
        private List<string> keys;
        private static string response,request;
        private static string prevrequest="",prevresponse="";
        static string topic="";
        Response res = new Response();
        Token tokn = new Token();
        private int count=0;
        
        public Request()
        {
            keys = new List<string>();
        }

        public void save_prev_input(string usereq)
        {
            prevrequest = usereq;
        }
        
        public void save_prev_output(string usereq)
        {
            prevresponse = response;
        }
        
        public void find_topic()
        {
            topic= request.Split(' ').Last();
        }

        public static bool null_input()
        {
            return (request.Length == 0 && prevrequest.Length != 0);
        }

        public static bool null_input_repetition()
        {
            return (request.Length == 0 && prevrequest.Length == 0);
        }

        public static bool user_repeat()  
        {    
            return (prevrequest.Length > 0 && (request.Equals(prevrequest))); 
        }

        public static bool same_input()  
        {
		return (request.Length > 0 && request == prevrequest);
        }

        public void addingkeys()
        {
            keys.Clear();
            keys = tokn.Tokens(request);
        }

        public string reqst(string text)
        {
            request = text;
            find_topic();
            if (null_input())
            {
                count = Convert.ToInt32(ConfigurationManager.AppSettings["null"]);
                response = res.monoresponse(count);
                return response;
            }
            else if (null_input_repetition())
            {
                count = Convert.ToInt32(ConfigurationManager.AppSettings["nullagain"]);
                response = res.monoresponse(count);
                return response;
            }
            else if (user_repeat())
            {
                if (same_input())
                {
                    count = Convert.ToInt32(ConfigurationManager.AppSettings["repeat"]);
                    response = res.monoresponse(count);
                    return response;
                }
            }
            else
            {
                tokn.trikeyword();
                addingkeys();
                if (keys.Count >=3)
                {    
                    response = matchtrikeys();
                    if (string.IsNullOrEmpty(response) || string.IsNullOrWhiteSpace(response))
                    {
                        response = bikey();
                        return response;
                    }
                    else
                    {
                        return response;
                    }
                }
                else
                {
                    response = bikey();
                    return response;
                }
            }
            return response;
        }

        public string bikey()
        {
            String rs="";
            tokn.bikeyword();
            addingkeys();
            if (keys.Count >= 2)
            {
                rs = matchbikeys();
                if (string.IsNullOrEmpty(rs) || string.IsNullOrWhiteSpace(rs))
                {
                    rs = monokey();
                    return rs;
                }
                else
                {
                    return rs;
                }
            }
            else
            {
                rs = monokey();
                return rs;
            }
        }

        public string monokey()
        {
        String rs="";
        tokn.monokeyword();
        count = tokn.Findkeys(request);
            if (count != 0)
            {
            rs = res.monoresponse(count);
            return rs;
            }  
            else
            {
                count = Convert.ToInt32(ConfigurationManager.AppSettings["outofbox"]);
            rs = res.monoresponse(count);
            return rs;
            }
        }

        public String Replaces(String str)
        {
            string replacedstr = str;
            if (str.Contains("*"))
            {
                replacedstr = str.Replace("*", HttpContext.Current.Session["UserName"].ToString());
            }
            if (str.Contains("@"))
            {
                replacedstr = str.Replace("@", HttpContext.Current.Session["BotName"].ToString());
            }
            if(str.Contains("#"))
            {
                replacedstr = str.Replace("#", topic);
            }
            if (str.Contains("&"))
            {
                replacedstr = str.Replace("&",prevresponse);
            }
            if (str.Contains("^"))
            {
                replacedstr = str.Replace("^",prevrequest);
            }
            if (str.Contains("%"))
            {
                replacedstr = str.Replace("%",HttpContext.Current.Session["Gender"].ToString());
            }
            return replacedstr;
        }

        public string matchtrikeys()
        {
            string str="";
            if (keys.Count == 4)
            {
                str = res.triresponse(keys[0], keys[1], keys[2]);
                if (string.IsNullOrEmpty(str) || string.IsNullOrWhiteSpace(str))
                {
                    str = res.triresponse(keys[1], keys[2], keys[3]);
                }
                else
                {
                    return str;
                }
                if (string.IsNullOrEmpty(str) || string.IsNullOrWhiteSpace(str))
                {
                    str = res.triresponse(keys[0], keys[2], keys[3]);
                }
                else
                {
                    return str;
                }
                if (string.IsNullOrEmpty(str) || string.IsNullOrWhiteSpace(str))
                {
                    str = res.triresponse(keys[0], keys[1], keys[3]);
                }
                else
                {
                    return str;
                }
            }
            else if (keys.Count == 3)
            {
                str = res.triresponse(keys[0], keys[1], keys[2]);
            }
            return str;
        }

        public string matchbikeys()
        {
            string str = "";
            str = res.biresponse(keys[0], keys[1]);
            if (keys.Count == 3)
            {
                if (string.IsNullOrEmpty(str) || string.IsNullOrWhiteSpace(str))
                {
                    str = res.biresponse(keys[1], keys[2]);
                }
                else
                {
                    return str;
                }
                if (string.IsNullOrEmpty(str) || string.IsNullOrWhiteSpace(str))
                {
                    str = res.biresponse(keys[0], keys[2]);
                }
                else
                {
                    return str;
                }
            }
            return str;
        }
  }
}