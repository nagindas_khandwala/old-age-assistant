﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace MyBot.Chatbot_Page
{
    public class Response
    {

        SqlConnection con;
        SqlCommand cmd;
        private String resp;
        
        public String monoresponse(int count)
        {
            connect();
            cmd = new SqlCommand("procresponse", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter ID = new SqlParameter("@id", count);
            cmd.Parameters.Add(ID);
            resp = (string)cmd.ExecuteScalar();
            con.Close();
            return resp;
        }

        public string triresponse(string k1, string k2, string k3)
        {
            connect();
            cmd = new SqlCommand("procTriresponse", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter key1 = new SqlParameter("@key1", k1);
            SqlParameter key2 = new SqlParameter("@key2", k2);
            SqlParameter key3 = new SqlParameter("@key3", k3);
            cmd.Parameters.Add(key1);
            cmd.Parameters.Add(key2);
            cmd.Parameters.Add(key3);
            resp = (string)cmd.ExecuteScalar();
            con.Close();
            return resp;
        }

        public string biresponse(string k1, string k2)
        {
            connect();
            cmd = new SqlCommand("procBiresponse", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter key1 = new SqlParameter("@key1", k1);
            SqlParameter key2 = new SqlParameter("@key2", k2);
            cmd.Parameters.Add(key1);
            cmd.Parameters.Add(key2);
            resp = (string)cmd.ExecuteScalar();
            con.Close();
            return resp;
        }

        public void connect()
        {
            String strcon = ConfigurationManager.ConnectionStrings["con"].ConnectionString;
            con = new SqlConnection(strcon);
            con.Open();
        }
    }
}