﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace MyBot.Chatbot_Page
{
    public class Normalizer
    {
        private String delim = "?!.,'";
        SqlConnection con;
        SqlCommand cmd;
        SqlDataReader dr;
        string normalizedtext="";
        Dictionary<string, string> normalize;
        
        public Normalizer()
        {
            normalize = new Dictionary<string, string>();
            String strcon = ConfigurationManager.ConnectionStrings["con"].ConnectionString;
            con = new SqlConnection(strcon);
            con.Open();
            cmd = new SqlCommand("select rawdata,normalizedata from Normalizer", con);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                normalize.Add(dr[0].ToString(), dr[1].ToString());
            }
            con.Close();
            dr.Close();
        }

        public string normalized(string text)
        {
            string[] words = text.Split(' ');
            foreach (string s in words)
            {
                if (normalize.ContainsKey(s))
                {
                    normalizedtext+=normalize[s]+" ";
                }
                else
                {
                    normalizedtext+=s+" ";
                }
            }
            normalizedtext = normalizedtext.TrimEnd(' ');
            return normalizedtext;
        }

        private Boolean isPunc(char ch)
        {
            return delim.IndexOf(ch) != -1;
        }
        
        public String cleanString(String str)
        {
            String temp = "";
            char prevChar = ' ';
            for (int i = 0; i < str.Length; ++i)
            {
                if ((str.ElementAt(i) == ' ' && prevChar == ' ') || !isPunc(str.ElementAt(i)))
                {
                    temp += str.ElementAt(i);
                    prevChar = str.ElementAt(i);
                }
                else if (prevChar != ' ' && isPunc(str.ElementAt(i)))
                {
                    temp += "";
                }

            }
            return temp;
        }

    }
}