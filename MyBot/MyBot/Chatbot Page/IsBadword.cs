﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
namespace MyBot.Chatbot_Page
{
    public class IsBadword
    {
        SqlConnection con;
        SqlCommand cmd, cmd1;
        private int count = 0;
        private Dictionary<String, int> badword;
        private String responses;

        public IsBadword()
        {
            int i = 0;
            SqlDataReader reader;
            badword = new Dictionary<String, int>();
            connect();
            String cmdStr = "Select Badwords from [Badword]";
            cmd = new SqlCommand(cmdStr, con);
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                badword.Add(reader.GetValue(0).ToString(), i++);
            }
            con.Close();
            reader.Close();
        }

        public int BadWord(string text)
        {
            string[] words = text.Split(' ');
            foreach (String s in words)
            {
                if (badword.ContainsKey(s))
                {
                    count += 1;
                }
            }
            return count;
        }

        public String find()
        {
            int i = Convert.ToInt32(ConfigurationManager.AppSettings["badword"]);
            String strcon = ConfigurationManager.ConnectionStrings["con"].ConnectionString;
            con = new SqlConnection(strcon);
            con.Open();

            cmd1 = new SqlCommand("procresponse", con);
            cmd1.CommandType = CommandType.StoredProcedure;
            SqlParameter ID = new SqlParameter("@id", i);
            cmd1.Parameters.Add(ID);
            responses = (string)cmd1.ExecuteScalar();
            con.Close();
            return responses;
        }

        public void connect()
        {
            String strcon = ConfigurationManager.ConnectionStrings["con"].ConnectionString;
            con = new SqlConnection(strcon);
            con.Open();
        }
    }
}