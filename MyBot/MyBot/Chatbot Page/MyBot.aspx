﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Bot.Master" AutoEventWireup="true"
    CodeBehind="MyBot.aspx.cs" Inherits="MyBot.MyBot" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="Stylesheet" type="text/css" href="../StyleSheet/StyleBot.css" />
    <script type="text/javascript">
        window.onload = function () {
            var div = document.getElementById("ContentPlaceHolder1_MainChat");
            div.scrollTop = div.scrollHeight;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <div class="form card">
        <h3>
            <asp:Label ID="lblbotname" runat="server"></asp:Label></h3>
        <div class="innerform">
            <form action="" method="post">
            <div id="Div1" runat="server" class="chatbox">
                <div id="MainChat" runat="server" class="chatlogs">
                    <div runat="server" id="botchat" class="chat bot">
                        <div id="Div3" runat="server" class="user-photo">
                            <asp:Image ID="Image1" runat="server" class="img" ImageUrl="~/Images/FileBot-icon.png" /></div>
                        <asp:Label ID="Label1" runat="server" class="chat-message"></asp:Label>
                    </div>
                </div>
                <div runat="server" id="form" class="chat-form">
                    <asp:TextBox ID="txtuser" class="textbox" runat="server" placeholder="Enter Text and Hit <Enter> to Chat"
                        OnTextChanged="txtuser_TextChanged" AutoPostBack="True"></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="txtuser_FilteredTextBoxExtender" runat="server"
                        Enabled="True" FilterType="LowercaseLetters,UppercaseLetters,Custom" ValidChars=" ?!.,'" TargetControlID="txtuser">
                    </asp:FilteredTextBoxExtender>
                    <asp:AutoCompleteExtender ID="txtuser_AutoCompleteExtender" runat="server" DelimiterCharacters=" "
                        ServiceMethod="GetComplete" MinimumPrefixLength="3" EnableCaching="true" CompletionInterval="100"
                        CompletionSetCount="10" FirstRowSelected="true" TargetControlID="txtuser" CompletionListCssClass="completionList"
                         CompletionListHighlightedItemCssClass="itemHighlighted" CompletionListItemCssClass="listItem">
                    </asp:AutoCompleteExtender>
                </div>
            </div>
            </form>
        </div>
    </div>
</asp:Content>
