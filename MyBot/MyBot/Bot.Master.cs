﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace MyBot
{
    public partial class Bot : System.Web.UI.MasterPage
    {
        SqlConnection con;
        SqlCommand cmd;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserName"] != null)
            {
                Panel2.Visible = true;
                Panel1.Visible = false;
            }
            else
            {
                Panel2.Visible = false;
                Panel1.Visible = true;

            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            LogChats();
            Session.Abandon();
            FormsAuthentication.SignOut();
            Panel2.Visible = false;
            Panel1.Visible = true;
            Response.Redirect("~/Home");
        }
     
        private void LogChats()
        {
            connect();
            string dash = "-";
            cmd = new SqlCommand("procLogChats", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter USER = new SqlParameter("@username",dash);
            SqlParameter REQUEST = new SqlParameter("@request", dash);
            SqlParameter RESPONSE = new SqlParameter("@response", dash);
            SqlParameter DATE_TIME = new SqlParameter("@date_time", dash);

            cmd.Parameters.Add(USER);
            cmd.Parameters.Add(REQUEST);
            cmd.Parameters.Add(RESPONSE);
            cmd.Parameters.Add(DATE_TIME);

            cmd.ExecuteScalar();
            con.Close();
        }

        public void connect()
        {
            String conStr = ConfigurationManager.ConnectionStrings["con"].ConnectionString;
            con = new SqlConnection(conStr);
            con.Open();
        }

    }
}