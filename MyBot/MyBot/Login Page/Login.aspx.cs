﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Security;

namespace MyBot
{
    public partial class Login : System.Web.UI.Page
    {
        SqlConnection con;
        SqlCommand cmd;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserName"] != null)
            {
                Response.Redirect("~/HomePage/Home.aspx");
            }
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            if (DBinsert())
            {
                Session["UserName"] = txtnickname.Text;
                Session["BotName"] = txtbotname.Text;
                Session["Gender"] = Genderlist.SelectedItem.Value;
                FormsAuthentication.RedirectFromLoginPage(txtbotname.Text,true);
            }
            else
            {
                Label1.Text = "Your And Bot Name Should Not Be Same.";
            }
        }
        
        private bool DBinsert()
        {
            connect();
            cmd = new SqlCommand("proclogin", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlParameter ID = new SqlParameter("@id", (Guid.NewGuid().ToString()).Substring(0,10));
            SqlParameter BOT = new SqlParameter("@botname", txtbotname.Text);
            SqlParameter USER = new SqlParameter("@username", txtnickname.Text);
            SqlParameter GENDER = new SqlParameter("@gender", Genderlist.SelectedItem.Value);
            SqlParameter Date_Time = new SqlParameter("@date_time", DateTime.Now.ToString());

            cmd.Parameters.Add(ID);
            cmd.Parameters.Add(BOT);
            cmd.Parameters.Add(USER);
            cmd.Parameters.Add(GENDER);
            cmd.Parameters.Add(Date_Time);

            int returnvalue = (int)cmd.ExecuteScalar();
            return (returnvalue == 1);
         }
        
        public void connect()
        {
            String conStr = ConfigurationManager.ConnectionStrings["con"].ConnectionString;
            con = new SqlConnection(conStr);
            con.Open();
        }

    }
 }