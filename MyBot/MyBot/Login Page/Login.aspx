﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Bot.Master" AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="MyBot.Login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="Label1" class="lbl" runat="server" Text="" ForeColor="red"></asp:Label>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="form card">
        <h3>
            LOGIN</h3>
        <div class="innerform">
            <form id="form1" action="" method="post">
            <table class="tbl" align="center" frame="box">
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="botname" class="lbl" runat="server" Text="ChatBot's Name"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtbotname" placeholder="Enter ChatBot's Name" class="txt" runat="server"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="txtbotname_FilteredTextBoxExtender" runat="server"
                            Enabled="True" TargetControlID="txtbotname" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                            ValidChars=" ">
                        </asp:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="Requiredbotname" runat="server" ErrorMessage="ChatBot's Name is Required"
                            ForeColor="Red" ControlToValidate="txtbotname" class="valid" Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="nickname" class="lbl" runat="server" Text="Your Name"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtnickname" class="txt" placeholder="Enter Your Name" runat="server"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="txtnickname_FilteredTextBoxExtender" runat="server"
                            Enabled="True" TargetControlID="txtnickname" FilterType="UppercaseLetters,LowercaseLetters">
                        </asp:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredNickname" runat="server" ErrorMessage="Your Name is Required"
                            ForeColor="Red" class="valid" ControlToValidate="txtnickname" Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Gender" runat="server" class="lbl" Text="Gender"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="Genderlist" class="txt dropdown" runat="server">
                            <asp:ListItem Value="-1" Selected="True">Select Gender..</asp:ListItem>
                            <asp:ListItem>Male</asp:ListItem>
                            <asp:ListItem>Female</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredGender" runat="server" ErrorMessage="Gender is Required"
                            ForeColor="Red" class="valid" InitialValue="-1" Display="Dynamic" ControlToValidate="Genderlist"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Button ID="Submit" class="btn" runat="server" Text="Submit" OnClick="Submit_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
            </form>
        </div>
    </div>
</asp:Content>
