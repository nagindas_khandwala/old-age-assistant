﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace MyBot.Admin
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        SqlConnection con;
        SqlCommand cmd;

        protected void Button1_Click(object sender, EventArgs e)
        {
            connect();
            cmd = new SqlCommand("procallBiKeywords", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteScalar();
            con.Close();
            lblupdate.Text = "Updated Bi-Keywords Database.";
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            connect();
            cmd = new SqlCommand("procallTriKeywords", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteScalar();
            con.Close();
            lblupdate.Text = "Updated Tri-Keywords Database.";
        }
        public void connect()
        {
            String strcon = ConfigurationManager.ConnectionStrings["con"].ConnectionString;
            con = new SqlConnection(strcon);
            con.Open();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lblupdate.Text = "";
            if (Session["Admin"] == null)
            {
                Response.Redirect("~/Admin/AdminLogin.aspx");
            }
        }
    }
}