﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true"
    CodeBehind="ViewDictionaryWords.aspx.cs" Inherits="MyBot.Admin.ViewDictionaryWords" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
        DataKeyNames="ID" DataSourceID="SqlDataSource1" ShowFooter="True" OnPageIndexChanging="pageIndex"
        ForeColor="Black" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt">
        <Columns>
            <asp:TemplateField ShowHeader="False">
                <EditItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/save-icon.png" Height="30px"
                            Width="30px" />
                    </asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit">
                        <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/edit-button-image-68871.png"
                            Height="30px" Width="30px" />
                    </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <EditItemTemplate>
                    <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel">
                        <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/1488450444_f-cross_256.png"
                            Height="30px" Width="30px" />
                    </asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Delete">
                        <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/1488450444_f-cross_256.png"
                            Height="30px" Width="30px" />
                    </asp:LinkButton>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:LinkButton ValidationGroup="Insert" ID="btninsert" Text="Insert" CssClass="btninsert"
                        OnClick="Insert_Click" runat="server">
                    </asp:LinkButton>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ID" InsertVisible="False" SortExpression="ID">
                <EditItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Dictionary Words" SortExpression="words">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" CssClass="txt" runat="server" Text='<%# Bind("words") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Text="requird."
                        ControlToValidate="TextBox1" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("words") %>'></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtwords" CssClass="txt" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="Insert" ID="RequiredFieldValidator2"
                        runat="server" Text="requird." Font-Bold="true" ControlToValidate="txtwords"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                </FooterTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:AssistantBotConnectionString %>"
        DeleteCommand="DELETE FROM [Dictionary] WHERE [ID] = @ID" InsertCommand="INSERT INTO [Dictionary] ([words]) VALUES (@words)"
        SelectCommand="SELECT * FROM [Dictionary]" UpdateCommand="UPDATE [Dictionary] SET [words] = @words WHERE [ID] = @ID">
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="words" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="words" Type="String" />
            <asp:Parameter Name="ID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
