﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true"
    CodeBehind="Managing.aspx.cs" Inherits="MyBot.Admin.Managing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" ForeColor="Black"
        ShowFooter="True" OnPageIndexChanging="pageIndex" DataKeyNames="ID" CssClass="mGrid"
        PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt" DataSourceID="SqlDataSource1"
        AllowPaging="True" >
        <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
        <Columns>
            <asp:TemplateField ShowHeader="False">
                <EditItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/save-icon.png" Height="30px" Width="30px"/>
                    </asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit">
                        <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/edit-button-image-68871.png" Height="30px" Width="30px"/>
                    </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <EditItemTemplate>
                    <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel">
                        <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/1488450444_f-cross_256.png" Height="30px" Width="30px"/>
                    </asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Delete">
                        <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/1488450444_f-cross_256.png" Height="30px" Width="30px" />
                    </asp:LinkButton>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:LinkButton ValidationGroup="Insert" ID="btninsert" Text="Insert" CssClass="btninsert" OnClick="Insert_Click" runat="server">
                    </asp:LinkButton>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ID" InsertVisible="False" SortExpression="ID">
                <EditItemTemplate>
                    <asp:Label ID="Label1"  runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1"  runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Keyword" SortExpression="Keyword">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" CssClass="txt" runat="server" Text='<%# Bind("Keyword") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Text="requird."
                        ControlToValidate="TextBox1" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2"  runat="server" Text='<%# Bind("Keyword") %>'></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtkeyword" CssClass="txt" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="Insert" ID="RequiredFieldValidator1"
                        runat="server" Text="requird." Font-Bold="true" ControlToValidate="txtkeyword"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Response_ID" SortExpression="Response_ID">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" CssClass="txt" runat="server" Text='<%# Bind("Response_ID") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Text="requird."
                        ControlToValidate="TextBox2" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3"  runat="server" Text='<%# Bind("Response_ID") %>'></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtrespid" CssClass="txt" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="Insert" ID="RequiredFieldValidator2"
                        runat="server" Text="requird." Font-Bold="true" ControlToValidate="txtrespid"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Category" SortExpression="Category">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" CssClass="txt" runat="server" Text='<%# Bind("Category") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Font-Bold="true"
                        Text="requird." ControlToValidate="TextBox3" ForeColor="Red"></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label4"  runat="server" Text='<%# Bind("Category") %>'></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtcategory" CssClass="txt" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="Insert" ID="RequiredFieldValidator3"
                        runat="server" Font-Bold="true" Text="requird." ControlToValidate="txtcategory"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                </FooterTemplate>
            </asp:TemplateField>
        </Columns>
        <PagerStyle CssClass="pgr"></PagerStyle>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:AssistantBotConnectionString %>"
        DeleteCommand="DELETE FROM [Keywords] WHERE [ID] = @ID" 
        InsertCommand="INSERT INTO [Keywords] ([Keyword], [Response_ID], [Category]) VALUES (@Keyword, @Response_ID, @Category)"
        SelectCommand="SELECT * FROM [Keywords]" 
        UpdateCommand="UPDATE [Keywords] SET [Keyword] = @Keyword, [Response_ID] = @Response_ID, [Category] = @Category WHERE [ID] = @ID">
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Keyword" Type="String" />
            <asp:Parameter Name="Response_ID" Type="Int32" />
            <asp:Parameter Name="Category" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Keyword" Type="String" />
            <asp:Parameter Name="Response_ID" Type="Int32" />
            <asp:Parameter Name="Category" Type="Int32" />
            <asp:Parameter Name="ID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
