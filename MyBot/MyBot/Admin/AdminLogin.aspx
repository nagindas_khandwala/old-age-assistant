﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminLogin.aspx.cs" Inherits="MyBot.Admin.AdminLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../StyleSheet/Style.css" rel="stylesheet" type="text/css" />
    <title>Admin Login</title>
</head>
<body>
    <form id="form1" runat="server">
    <br /><br />
    <div style="text-align:center; font-family: Verdana; font-size: large; font-weight: bold;">
        <b>Username:</b> elias
        <br />
        <b>Password:</b> gomes
    </div>
    <br /><br /><br />
    <div style="text-align:center;">
        <asp:Label runat="server" CssClass="lbl" ForeColor="Red" ID="lblerror" />
    </div>
    <br /><br />
    <div style=" padding-left:300px; padding-right:300px">
    <table class="card mGrid" align="center">
        <tr>
            <td colspan="2" style="font-family: Verdana; font-size: large; font-weight: bold;
                background-color: #4CAF50">
                <span>Admin Login</span>
            </td>
        </tr>
        <tr>
            <td>
                <span class="lbl">User Name</span>
            </td>
            <td>
                <asp:TextBox runat="server" ID="username" CssClass="txt" Width="100%" />
            </td>
        </tr>
        <tr>
            <td>
                <span class="lbl">Password</span>
            </td>
            <td>
                <asp:TextBox runat="server" ID="password" CssClass="txt" Width="100%" TextMode="password" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="Button1" runat="server" Text="Login" CssClass="btn" OnClick="Button1_Click" />
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
