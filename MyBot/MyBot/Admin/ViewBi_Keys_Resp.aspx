﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true"
    CodeBehind="ViewBi_Keys_Resp.aspx.cs" Inherits="MyBot.Admin.ViewBi_Keys_Resp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
        DataKeyNames="ID" DataSourceID="SqlDataSource1" ShowFooter="True" OnPageIndexChanging="pageIndex"
        ForeColor="Black" CssClass="mGrid" PagerStyle-CssClass="pgr" AlternatingRowStyle-CssClass="alt">
        <Columns>
            <asp:TemplateField ShowHeader="False">
                <EditItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/save-icon.png" Height="30px"
                            Width="30px" />
                    </asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit">
                        <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/edit-button-image-68871.png"
                            Height="30px" Width="30px" />
                    </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ShowHeader="False">
                <EditItemTemplate>
                    <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel">
                        <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/1488450444_f-cross_256.png"
                            Height="30px" Width="30px" />
                    </asp:LinkButton>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Delete">
                        <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/1488450444_f-cross_256.png"
                            Height="30px" Width="30px" />
                    </asp:LinkButton>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:LinkButton ValidationGroup="Insert" ID="btninsert" Text="Insert" CssClass="btninsert"
                        OnClick="Insert_Click" runat="server">
                    </asp:LinkButton>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ID" InsertVisible="False" SortExpression="ID">
                <EditItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("ID") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Keyword-1" SortExpression="keyword1">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" CssClass="txt" runat="server" Text='<%# Bind("keyword1") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Text="requird."
                        ControlToValidate="TextBox1" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("keyword1") %>'></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtkeyword1" CssClass="txt" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="Insert" ID="RequiredFieldValidator5"
                        runat="server" Text="requird." Font-Bold="true" ControlToValidate="txtkeyword1"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Keyword-2" SortExpression="keyword2">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" CssClass="txt" runat="server" Text='<%# Bind("keyword2") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Text="requird."
                        ControlToValidate="TextBox2" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("keyword2") %>'></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtkeyword2" CssClass="txt" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="Insert" ID="RequiredFieldValidator7"
                        runat="server" Text="requird." Font-Bold="true" ControlToValidate="txtkeyword2"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Response" SortExpression="Response">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" CssClass="txt" runat="server" Text='<%# Bind("Response") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Text="requird."
                        ControlToValidate="TextBox3" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Response") %>'></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtresponse" CssClass="txt" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="Insert" ID="RequiredFieldValidator8"
                        runat="server" Text="requird." Font-Bold="true" ControlToValidate="txtresponse"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Category" SortExpression="Category">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" CssClass="txt" runat="server" Text='<%# Bind("Category") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Text="requird."
                        ControlToValidate="TextBox4" Font-Bold="true" ForeColor="Red"></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("Category") %>'></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="txtcategory" CssClass="txt" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ValidationGroup="Insert" ID="RequiredFieldValidator9"
                        runat="server" Text="requird." Font-Bold="true" ControlToValidate="txtcategory"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                </FooterTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:AssistantBotConnectionString %>"
        DeleteCommand="DELETE FROM [BiKeywords] WHERE [ID] = @ID" InsertCommand="INSERT INTO [BiKeywords] ([keyword1], [keyword2], [Response], [Category]) VALUES (@keyword1, @keyword2, @Response, @Category)"
        SelectCommand="SELECT * FROM [BiKeywords]" UpdateCommand="UPDATE [BiKeywords] SET [keyword1] = @keyword1, [keyword2] = @keyword2, [Response] = @Response, [Category] = @Category WHERE [ID] = @ID">
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="keyword1" Type="String" />
            <asp:Parameter Name="keyword2" Type="String" />
            <asp:Parameter Name="Response" Type="String" />
            <asp:Parameter Name="Category" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="keyword1" Type="String" />
            <asp:Parameter Name="keyword2" Type="String" />
            <asp:Parameter Name="Response" Type="String" />
            <asp:Parameter Name="Category" Type="Int32" />
            <asp:Parameter Name="ID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <br />
    <asp:Label ID="Label6" runat="server" CssClass="lbl" ForeColor="Green" Text="Note : Update Bi-Keywords Database to use the New Keywords Inserted."></asp:Label>
</asp:Content>
