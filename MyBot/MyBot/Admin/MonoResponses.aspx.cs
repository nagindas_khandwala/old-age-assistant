﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyBot.Admin
{
    public partial class MonoResponses : System.Web.UI.Page
    {
        protected void Insert_Click(object sender, EventArgs e)
        {
            SqlDataSource1.InsertParameters["Responses"].DefaultValue = ((TextBox)GridView1.FooterRow.FindControl("txtresponse")).Text;
            SqlDataSource1.InsertParameters["Resp_ID"].DefaultValue = ((TextBox)GridView1.FooterRow.FindControl("txtrespid")).Text;

            SqlDataSource1.Insert();
        }

        protected void pageIndex(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
        }
    }
}