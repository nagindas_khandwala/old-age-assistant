﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyBot.Admin
{
    public partial class ViewBi_Keys_Resp : System.Web.UI.Page
    {
        protected void Insert_Click(object sender, EventArgs e)
        {
            SqlDataSource1.InsertParameters["keyword1"].DefaultValue = ((TextBox)GridView1.FooterRow.FindControl("txtkeyword1")).Text;
            SqlDataSource1.InsertParameters["keyword2"].DefaultValue = ((TextBox)GridView1.FooterRow.FindControl("txtkeyword2")).Text;
            SqlDataSource1.InsertParameters["Response"].DefaultValue = ((TextBox)GridView1.FooterRow.FindControl("txtresponse")).Text;
            SqlDataSource1.InsertParameters["Category"].DefaultValue = ((TextBox)GridView1.FooterRow.FindControl("txtcategory")).Text;

            SqlDataSource1.Insert();
        }

        protected void pageIndex(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
        }

    }
}