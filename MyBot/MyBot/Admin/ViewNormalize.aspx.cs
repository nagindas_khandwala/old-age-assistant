﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MyBot.Admin
{
    public partial class ViewNormalize : System.Web.UI.Page
    {
        protected void Insert_Click(object sender, EventArgs e)
        {
            SqlDataSource1.InsertParameters["rawdata"].DefaultValue = ((TextBox)GridView1.FooterRow.FindControl("txtrawdata")).Text;
            SqlDataSource1.InsertParameters["normalizedata"].DefaultValue = ((TextBox)GridView1.FooterRow.FindControl("txtnormalizedata")).Text;

            SqlDataSource1.Insert();
        }

        protected void pageIndex(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
        }

    }
}