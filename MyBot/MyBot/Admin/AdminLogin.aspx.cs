﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

namespace MyBot.Admin
{
    public partial class AdminLogin : System.Web.UI.Page
    {
        string uname, pass;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Admin"] != null)
            {
                Response.Redirect("~/Admin/Managing.aspx");
            }
            else
            {
                uname = ConfigurationManager.AppSettings["username"];
                pass = ConfigurationManager.AppSettings["password"];
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (uname.Equals(username.Text) & pass.Equals(password.Text))
            {
                Session["Admin"] = uname;
                Response.Redirect("~/Admin/Managing.aspx");
            }
            else
            {
                lblerror.Text = "Invalid Username or Password";
            }
        }
    }
}