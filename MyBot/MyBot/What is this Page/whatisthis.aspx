﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Bot.Master" AutoEventWireup="true"
    CodeBehind="whatisthis.aspx.cs" Inherits="MyBot.What_is_this.whatisthis" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="form card">
        <h3>About Us</h3>
        <div class="innerform" align="center">
            <br />
            <p>
                Our Chatbot was created by Elias Gomes.</p>
            <p>
                His styles vary from commercial, vocal trance to hard banging dance music.</p>
            <p>
                Since 2015, he has been interested in Artificial Intelligence and so AssistantBot
                was born.</p>
            <p>
                Feel free to send me an email using the form below if you have any questions or
                problems with Our ChatBot.</p>
            <p>
                Any feedback on her would be great too.</p>
            <p>
                Don't send me any spam or anything rude though please.</p>
            <p>
                I already get enough of that in my Inbox :)</p>
            <br />
            <br />
            <br />
            <p>Mobile/text : +91-7666358584</p> 
            <p>Email - ID : rohitgomes85@gmail.com</p>
        </div>
    </div>
</asp:Content>
