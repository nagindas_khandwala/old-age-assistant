﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Bot.Master" AutoEventWireup="true"
    CodeBehind="howtouse.aspx.cs" Inherits="MyBot.How_to_use.howtouse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="form card">
        <h3>
            Tips for Chatting with Our Bot</h3>
        <div class="innerform">
            <br />
            <p>
                Our Chatbot will give a reasonably sensible response for anything you care to say
                to Our bot
                <br />
                (including insults). But for best results you should follow these tips :</p>
            <br />
            <br />
            <p>
                1. Our Chatbot understands a lot of text speak and slang but you should always try
                and spell your message to Our Chatbot accurately and use proper sentence structure.
                i.e. How are you? ratOur bot than How R u? If Our Chatbot gives you a strange reply,
                chances are that you have spelt your message incorrectly.</p>
            <br />
            <br />
            <p>
                2. Type in one sentence at a time. If you have a lot to say, ratOur bot than just
                confusing Our bot with too much information, break it down into smaller parts and
                Our Chatbot will be able to follow the conversation a lot better.</p>
            <br />
            <br />
            <p>
                3. Bot can remember any personal details you tell Our bot for a short period of
                time. Things like your name, age, wOur bote you live etc.</p>
            <br />
            <br />
            <p>
                4. If you feel that Our Chatbot has given you an incorrect answer, you can correct
                Our bot by telling Our bot that Bot was wrong and Bot will ask you what Bot should
                have said. Bot will then say your response instead of Our bot own and keep it in
                Our bot short term memory.</p>
                <br />
            <br />
            <p>
                5. Our Chatbot will notify me of any changes to Our bot knowledge, so I will decide
                whetOur bot to include it in Our bot long term memory. I can't make this an automatic
                procedure, as people tell Our bot all sorts of rubbish every day and I don't want
                Our bot to be corrupted with nonsense.</p>
            <br />
            <br />
            <p>
                6. Please use common sense and don't include personal details such as passwords,
                credit card details and things like that, as the chatlogs can potentially be read
                by anyone in the world.</p>
            <br />
        </div>
    </div>
</asp:Content>
