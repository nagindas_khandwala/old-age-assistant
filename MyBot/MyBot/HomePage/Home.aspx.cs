﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace MyBot.HomePage
{
    public partial class Home : System.Web.UI.Page
    {
        SqlConnection con;
        SqlCommand cmd, cmd1;
        int bots, interactions;
        protected void Page_Load(object sender, EventArgs e)
        {
            connect();
            Label1.Text = bots + " K";
            Label2.Text = interactions + " B";
        }
        private void connect()
        {
            String strcon = ConfigurationManager.ConnectionStrings["con"].ConnectionString;
            con = new SqlConnection(strcon);
            con.Open();
            cmd = new SqlCommand("Select count(distinct(BotName)) from [Login]", con);
            bots = (int)cmd.ExecuteScalar();
            cmd1 = new SqlCommand("Select count(*) from [Login]", con);
            interactions = (int)cmd1.ExecuteScalar();
            con.Close();
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            Response.Redirect("~/What is this Page/whatisthis.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Login Page/Login.aspx");
        }
    }
}