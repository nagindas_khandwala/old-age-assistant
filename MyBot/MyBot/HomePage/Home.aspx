﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Bot.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="MyBot.HomePage.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="card" align="center">
        <asp:Label ID="Label1" CssClass="lbl" runat="server"></asp:Label>
        <p>Chatbots Created</p>
        <asp:Label ID="Label2" CssClass="lbl" runat="server"></asp:Label>
        <p>Interactions</p>
    </div>
    <br />
        <div align="center" class="card" style="background-position: right top; background-image: url('http://localhost:1326/Images/robot.jpg'); background-repeat: no-repeat;">
         	<br /><br /><br /><br /><br /><br />
            <p>You need never feel lonely again!</p>
            <p>Our ChatBot is your new virtual friend</p>
            <p>and is here 24 hours a day just to talk to you.</p>
            <p>Our Bot learns by experience,</p>
            <p>So the more people talk,</p>
            <p>the smarter it will becomes.</p>
            <br /><br /><br /><br /><br /><br />
        </div>
        <br />
    <div class="form card">
    <h3>Get Started</h3>
    <div class="innerform" align="center">
     <p>To begin talking to our Bot,</p>
     <p>you have to Log in First.</p>
     <p>Just Click on the button below,</p>
     <p>You can also click the Login Menu on the Menu bar.</p>
        <asp:Button ID="Button2" runat="server" CssClass="rippleripple" 
            Text="Log In Free" CausesValidation="False" onclick="Button2_Click" />
    </div>
    </div>
    <br /><br />
    <div class="form card">
    <center><h3>About Us</h3></center>
    <div class="innerform" align="center">
    <p>Assistant Bot is a great new service out there for you to use. </p>
    <p>It basically allows you to talk to your own personal chatbot,</p>
    <p>with your own Chatbot Name.</p> 
    <p>But why should you do that and how is Assistant Bot going to help you </p>
    <p>Achieve anything other than entertainment?</p> 
    <p>Read on to find out more.</p>
        <asp:Button ID="Button1" runat="server" CssClass="rippleripple" Text="About Us" 
            onclick="Button1_Click1" CausesValidation="False" />
        </div>
    </div>
    <br /><br />
</asp:Content>
