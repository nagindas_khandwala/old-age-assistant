﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Text;
using System.Drawing.Imaging;

namespace MyBot.Contact_us
{
    public partial class captcha : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            Bitmap bmp = new Bitmap(Server.MapPath("~//Images//captcha.jpg"));
            MemoryStream mstr = new MemoryStream();
            int width = bmp.Width;
            int height = bmp.Height;
            String Fnt = "Arial";

            String text = Request.Cookies["captcha"]["value"];
            Bitmap bitmp = new Bitmap(bmp, new Size(width, height));
            Graphics g = Graphics.FromImage(bitmp);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            int xcpr = width - 150;
            int ycpr = height - 50;

            Rectangle rect;
            int fntsize = 45;
            Font ft = new Font(Fnt, fntsize, FontStyle.Italic);
            rect = new Rectangle(xcpr, ycpr, 0, 0);
            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Near;
            format.LineAlignment = StringAlignment.Near;
            GraphicsPath path = new GraphicsPath();
            path.AddString(text, ft.FontFamily, (int)ft.Style, ft.Size, rect, format);

            HatchBrush brush = new HatchBrush(HatchStyle.LargeConfetti, Color.FromName("White"), Color.FromName("White"));
            g.FillPath(brush, path);
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "image/jpeg";
            bitmp.Save(mstr, ImageFormat.Png);
            bmp.Dispose();
            ft.Dispose();
            brush.Dispose();
            g.Dispose();

            mstr.WriteTo(HttpContext.Current.Response.OutputStream);
            bitmp.Dispose();
        }

    }
}