﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace MyBot
{
    public partial class Contact_Us : System.Web.UI.Page
    {

        SqlConnection con;
        SqlCommand cmd;

        private void captcha()
        {
            String text = (Guid.NewGuid().ToString()).Substring(0, 5);
            Response.Cookies["captcha"]["value"] = text;
            ImageCaptcha.ImageUrl = "captcha.aspx";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Label4.Text = "";
            captcha();
            if (Session["UserName"] != null)
            {
                txtname.Text = (String)Session["UserName"];
            }
        }

        protected void Submitbtn_Click(object sender, EventArgs e)
        {
            if (txtcaptcha.Text != Request.Cookies["captcha"]["value"])
            {
                txtcaptcha.Text = "";
                Label4.Text = "Invalid Captcha";
            }
            else
            {
                String conStr = ConfigurationManager.ConnectionStrings["con"].ConnectionString;
                con = new SqlConnection(conStr);
                con.Open();
                cmd = new SqlCommand("procContactUs", con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter NAME = new SqlParameter("@name", txtname.Text);
                SqlParameter EMAIL = new SqlParameter("@email", txtemail.Text);
                SqlParameter GENDER = new SqlParameter("@message", txtarea.Text);
                SqlParameter Date_Time = new SqlParameter("@date_time", DateTime.Now.ToString());

                cmd.Parameters.Add(NAME);
                cmd.Parameters.Add(EMAIL);
                cmd.Parameters.Add(GENDER);
                cmd.Parameters.Add(Date_Time);

                cmd.ExecuteScalar();
                con.Close();
                message();
            }
        }

        protected void btncaptcha_Click(object sender, EventArgs e)
        {
          captcha();
          txtcaptcha.Text = "";
        }

        public void message()
        {
                txtarea.Text = "";
                txtcaptcha.Text = "";
                txtemail.Text = "";
                txtname.Text = "";
                lblThanku.Text = "Thank You for Contacting Us,we will Respond you Soon.";
       }

       }
    }