﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Bot.Master" AutoEventWireup="true"
    CodeBehind="Contact-Us.aspx.cs" Inherits="MyBot.Contact_Us" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="form">
        <h3>
            CONTACT US</h3>
        <div class="innerform">
            <form action="" method="post">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <asp:Label ID="lblThanku" runat="server" CssClass="lbl"></asp:Label>
            <table class="tbl" align="center" frame="box">
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label1" class="lbl" runat="server" Text="Name" Width="147px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtname" class="txt" placeholder="Enter Your Name" runat="server"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="txtname_FilteredTextBoxExtender" runat="server"
                            Enabled="True" TargetControlID="txtname" FilterType="UppercaseLetters,LowercaseLetters,Custom"
                            ValidChars=" ">
                        </asp:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" class="valid"
                            ErrorMessage="RequiredFieldValidator" ControlToValidate="txtname" ForeColor="Red"
                            Text="Name is Required" Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Email" class="lbl"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtemail" runat="server" class="txt" placeholder="Enter Your Email"
                            TextMode="Email"></asp:TextBox>
                        <asp:FilteredTextBoxExtender ID="txtemail_FilteredTextBoxExtender" runat="server"
                            Enabled="True" TargetControlID="txtemail" FilterType="LowercaseLetters,Numbers,Custom"
                            ValidChars=".@">
                        </asp:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" class="valid" runat="server"
                            ErrorMessage="RequiredFieldValidator" Text="Email is Required" ControlToValidate="txtemail"
                            ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Invalid Email"
                            ForeColor="Red" class="valid" ControlToValidate="txtemail" Display="Dynamic"
                            ValidationExpression="\w+([-+.']\w+)*@(\w)+([-.]\w+)*\.\w+([-.](\w)+)*">
                        </asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" class="lbl" Text="Your Message"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtarea" runat="server" class="textarea" Height="200" placeholder="Enter Your Message"
                            Columns="40" TextMode="MultiLine" MaxLength="150"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" class="valid" runat="server"
                            ErrorMessage="RequiredFieldValidator" Text="Message is Required" ControlToValidate="txtarea"
                            ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:Image ID="ImageCaptcha" runat="server" />
                        <asp:LinkButton ID="btncaptcha" runat="server" OnClick="btncaptcha_Click" CausesValidation="False">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/image.gif" Width="50" Height="50" /></asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblcaptcha" class="lbl" runat="server" Text="Type the Code Same as Image"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtcaptcha" class="txt" placeholder="Enter the Above Code" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="RequiredFieldValidator"
                            ControlToValidate="txtcaptcha" class="valid" ForeColor="Red" Display="Dynamic">Captcha is Required</asp:RequiredFieldValidator>
                        <asp:Label ID="Label4" runat="server" class="lbl" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Button ID="Submitbtn" class="btn" runat="server" Text="Submit" OnClick="Submitbtn_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
            </form>
        </div>
    </div>
</asp:Content>
